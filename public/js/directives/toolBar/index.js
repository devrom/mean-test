const toolBar = angular.module('toolBarDirective', []);

toolBar.directive('toolBar', () => {
    return {
        restrict: 'A',
        replace: false,
        template: require('./template.html'),
        scope: {
            "isAuth": "<"
        }
    }
});