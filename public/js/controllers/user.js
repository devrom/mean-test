const userController = angular.module('userController', ['userService']);


userController.controller('userRegCtrl', ['User', '$scope', '$location', function (User, $scope, $location) {

    $scope.regUser = function () {

        const regData = this.regData;

        User.create(regData).then((res) => {

            const {success, message, id, username} = res.data;

            if (success) {
                alert('User was created by id: ' + id);
                $location.path('/');
            } else {
                alert(message);
            }
        });
    };


    $scope.getUserById = () => {

        User.getUserById().then((res) => {

            const {success, message} = res.data;

            if (success) {
                alert(message);
            } else {
                alert(message);
            }
        });
    };


    $scope.getUsers = () => {

        User.getUsers().then((res) => {

            const {success, message} = res.data;

            if (success) {
                alert(message);
            } else {
                alert(message);
            }
        });
    };

}]);