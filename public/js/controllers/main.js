angular
    .module('mainController', ['authService'])

    .controller('mainCtrl', mainCtrl);


function mainCtrl(AuthRequest, $rootScope, $scope, $location, $cookies) {

    $scope.login = function () {

        const vm = this;

        if (!checkLoginData(vm.loginData)) return;

        AuthRequest
            .login(vm.loginData)
            .then(res => {

                const {success, message, token, userData} = res.data;

                const expires = new Date();
                expires.setTime(expires.getTime() + (1000 * 60 * 60 * 24 * 7));

                if (success) {
                    $scope.isAuth = true;
                    $scope.user = userData;

                    alert(message);

                    $cookies.put('jwt', token, {'expires': expires});
                    $location.path('/profile');
                } else {
                    alert(message);
                }
            });
    };


    $scope.logout = () => {
        $scope.isAuth = false;
        $scope.user = {};
        $cookies.remove('jwt');
        $location.path('/');
    };

}


function checkLoginData(loginData) {
    if (typeof loginData === 'undefined') {
        alert("Empty data");
        return false;
    }

    if (loginData.username === '' || loginData.username == 0 || typeof loginData.username === 'undefined') {
        alert("Login is empty");
        return false;
    }

    if (loginData.password === '' || loginData.password == 0 || typeof loginData.password === 'undefined') {
        alert("Password is empty");
        return false;
    }

    return true;
}
