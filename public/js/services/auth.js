angular
    .module('authService', [])

    .factory('AuthInterceptor', AuthInterceptor)

    .factory('AuthRequest', AuthRequest);


function AuthInterceptor($q, $cookies, $location) {

    const AuthInterceptor = {};


    AuthInterceptor.responseError = response => {
        if (response.status === 401) {
            $cookies.remove('jwt');
            $location.path('/');
        }

        if (response.status === 403) {
            $location.path('/');
        }

        return $q.reject(response);
    };


    return AuthInterceptor;

}


function AuthRequest($http) {

    const AuthRequest = {};


    AuthRequest.create = regData => {
        return $http.post('/api/registration', regData);
    };


    AuthRequest.login = loginData => {
        return $http.post('/api/login', loginData);
    };


    return AuthRequest;

}
