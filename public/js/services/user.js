angular
    .module('userService', [])

    .factory('User', ($http, $routeParams) => {

        const UserFactory = {};


        UserFactory.getUserById = () => {
            const id = $routeParams.id;
            return $http.get('/api/user/' + id);
        };


        UserFactory.getUsers = () => {
            return $http.get('/api/user/s');
        };


        return UserFactory;

    });