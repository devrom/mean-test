angular
    .module('securityService', [])

    .factory('pageSecurity', pageSecurity);


function pageSecurity() {
    return {checkAccess};
}


function checkAccess(auth, role, roles) {
    return (!auth && !roles) || (auth && find(roles || [], role || '') + 1);
}


function find(array, value) {
    for (let i = 0; i < array.length; i++) {
        if (array[i] == value) return i;
    }

    return -1;
}