angular
    .module('myApp', [
        'ngRoute',
        'ngAnimate',
        'ngCookies',
        'ui.bootstrap',
        'mainController',
        'userController',
        'authService',
        'toolBarDirective',
        'securityService'
    ])

    .config(config)

    .run(run);


function config($httpProvider, $routeProvider, $locationProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');

    $locationProvider.html5Mode(true);

    $routeProvider
        .when('/', {
            template: require('../views/pages/home.html'),
            data: {
                pageTitle: 'Home'
            }
        })
        .when('/registration', {
            template: require('../views/pages/auth/registration.html'),
            data: {
                pageTitle: 'Registration'
            }
        })
        .when('/login', {
            template: require('../views/pages/auth/login.html'),
            data: {
                pageTitle: 'Login'
            }
        })
        .when('/profile', {
            template: require('../views/pages/user/profile.html'),
            data: {
                roles: ['admin', 'user'],
                pageTitle: 'Profile'
            }
        })
        .when('/test', {
            template: require('../views/pages/user/test.html'),
            data: {
                roles: ['admin'],
                pageTitle: 'Test'
            }
        })
        .otherwise({redirectTo: '/'});
}


function run($rootScope, $location, pageSecurity) {

    $rootScope.isAuth = true;
    $rootScope.curUserRole = 'admin';
    $rootScope.curUserName = 'Vasya';

    $rootScope.$on("$routeChangeStart", function (event, next, current) {

        if (!pageSecurity.checkAccess($rootScope.isAuth, $rootScope.curUserRole, next.$$route.data.roles)) {
            $location.path('/');
        }

    });


    $rootScope.$on("$routeChangeSuccess", function (event, next, current) {

        $rootScope.pageTitle = next.$$route.data.pageTitle + ' page';

    });

}
