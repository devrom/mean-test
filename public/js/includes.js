require('angular');
require('angular-cookies');
require('angular-route');
require('angular-animate');
require('angular-ui-bootstrap');


/** Controllers */
require('./controllers/main');
require('./controllers/user');


/** Services */
require('./services/auth');
require('./services/user');
require('./services/security');


/** Directives */
require('./directives/toolBar');


/** App */
require('./app');
