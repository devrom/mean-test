const PORT = process.env.PORT || 3000;
const path = require('path');

const express = require('express');
const app = express();

const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const mongoose = require('mongoose');
const configDB = require('./config/db');

const passport = require('passport');

const router = express.Router();


/** Вешаем все необходимые middleware */
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


/** Инициализация passport */
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport');


/** Роуты */
const appRoutes = require('./app/routes/index')(router);
app.use(express.static(__dirname + '/public'));
app.use('/api', appRoutes);


/** Соединение с базой */
mongoose.connect(configDB.database, (err) => {
    if (err) {
        console.log('Not connected to the database: ' + err);
    } else {
        console.log('Successfully connected to MongoDB');
    }
});


/** Все get запросы тянут для отображения index.html */
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/views/index.html'));
});


/** Запускаем backend server */
app.listen(PORT, () => {
    console.log('Server running on ' + PORT);
});
