module.exports = {getUsers, getUserById};

function getUsers(req, res) {
    res.json({success: true, message: 'All users'});
}

function getUserById(req, res) {
    res.json({success: true, message: 'Single user'});
}