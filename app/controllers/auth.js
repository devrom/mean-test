const jwt = require('jsonwebtoken');
const User = require('../models/user');

const configDB = require('../../config/db');
const message = require('../../config/message');

module.exports = {loginUser, createUser};

function loginUser(req, res) {
    const {username, password} = req.body;

    User.findOne({username: username}, (err, user) => {
        if (err) return res.json({success: false, message: message.auth.login.errors.schema + err});

        if (!user) return res.json({success: false, message: message.auth.login.errors.general});

        user.comparePassword(password, (err, isMatch) => {
            if (err) return res.json({success: false, message: message.auth.login.errors.comparePassword + err});

            if (!isMatch) return res.json({success: false, message: message.auth.login.errors.password});

            const userData = {id: user._id, username: user.username, role: user.role};

            /** Создаем токен */
            const token = jwt.sign(userData, configDB.secret, configDB.jwtOpts);

            res.json({success: true, message: message.auth.login.successes.general, userData, token});

        });

    });
}

function createUser(req, res) {
    const user = new User();

    const {username, password, email} = req.body;

    user.username = username;
    user.password = password;
    user.email = email;

    if (username == null || username == '' || password == null || password == '' || email == null || email == '') {
        res.json({success: false, message: message.auth.registration.errors.general});
    } else {
        user.save((err) => {
            if (err) return res.json({success: false, message: message.auth.registration.errors.schema + err});

            res.json({success: true, message: message.auth.registration.successes.general});
        });
    }
}