const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {type: String, lowercase: true, required: true, unique: true},
    password: {type: String, required: true},
    email: {type: String, required: true, lowercase: true, unique: true},
    role: {type: String, default: 'user'},
    created_at: {type: Date, default: Date.now}
});


/** Шифрование пароля перед сохранением пользователя в БД */
UserSchema.pre('save', function (next) {
    const user = this;
    bcrypt.hash(user.password, null, null, (err, hash) => {
        if (err) return next(err);
        user.password = hash;
        next();
    });
});


/** Сравнение паролей */
UserSchema.methods.comparePassword = function (password, callback) {
    bcrypt.compare(password, this.password, (err, isMatch) => {
        if (err) return callback(err, false);

        callback(null, isMatch);
    });
};


module.exports = mongoose.model('User', UserSchema);

