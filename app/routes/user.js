const ctrlUser = require('../controllers/user');
const passport = require('../../config/passport');
const permission = require('../../config/permission');

module.exports = (router) => {

    /** Получить всех юзеров */
    router.get('/users', passport, permission('admin'), ctrlUser.getUsers);


    /** Получить одного юзера */
    router.get('/user/:id', passport, permission('admin'), ctrlUser.getUserById);


    return router;
};
