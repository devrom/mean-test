module.exports = (router) => {

    require('./auth')(router);
    require('./user')(router);

    return router;
}