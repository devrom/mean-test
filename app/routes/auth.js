const ctrlAuth = require('../controllers/auth');
const passport = require('../../config/passport');

module.exports = (router) => {

    /** Авторизация юзера */
    router.post('/login', ctrlAuth.loginUser);


    /** Регистрация юзера */
    router.post('/registration', ctrlAuth.createUser);


    return router;
};