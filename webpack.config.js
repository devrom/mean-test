const webpack = require('webpack');
const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'source-map',
    entry: './public/js/includes.js',
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'main.js',
        publicPath: '/'
    },
    resolve: {
        modules: ["node_modules"],
        extensions: ['.js']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['env']
                        }
                    }
                ]
            },
            {
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader'
                }
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
    ],
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        watchContentBase: true,
        proxy: [
            {
                context: ['/'],
                target: 'http://localhost:3000',
                secure: false
            }
        ],
        port: 8080,
        overlay: {
            warnings: true,
            errors: true,
        },
        hot: true,
        inline: true
    }
};
