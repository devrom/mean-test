module.exports = {
    database: 'mongodb://localhost:27017/test',
    secret: 'secret',
    jwtOpts: {
        expiresIn: 1000 * 60 * 60 * 24 * 7 // 1 week
    }
};