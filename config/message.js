module.exports = {
    auth: {
        registration: {
            errors: {
                general: 'Ensure username, email, and password were provided',
                schema: 'User is not created: ',
            },
            successes: {
                general: 'User was created by id: '
            }
        },
        login: {
            errors: {
                general: 'User not found',
                schema: 'User not found, scheme error: ',
                password: 'Wrong password',
                comparePassword: 'Wrong password, compare error: ',
            },
            successes: {
                general: 'User was authorized'
            }
        },
        logout: {
            errors: {
                general: '',
            },
            successes: {
                general: ''
            }
        },
        loggedIn: {
            errors: {
                general: '',
            },
            successes: {
                general: ''
            }
        },
    },

    user: {},

    schedule: {
        get: {
            errors: {
                general: 'No schedule have been created yet'
            }
        },
        create: {
            errors: {
                general: 'Ensure day, timeStart and timeEnd were provided',
                schema: 'Schedule is not created: '
            },
            successes: {
                general: 'Schedule created with id: '
            }
        }
    }
};
